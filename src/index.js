#!/usr/bin/env node

const logger = require('./logger')('crawler');
const moment = require('moment');
const Pubsub = require('./pubsub');
const puller = require('./lib/puller');
const DB = require('./lib/mongodb');
const config = require('./config');
const Sentry = require('@sentry/node');

if (config.SENTRY_DSN) {
  Sentry.init({
    dsn: config.SENTRY_DSN
  });
}

async function main() {
  const ArgumentParser = require('argparse').ArgumentParser;
  const parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'Crawler'
  });
  parser.addArgument(['--feeds', '-f'], {help: 'set feed name'});

  const args = parser.parseArgs();
  const db = new DB();

  try {
    Sentry.captureEvent({
      message: "start"
    });

    await db.init(config.MONGODB_URL);

    const fromDate = moment().toDate();
    const toDate = moment().add(3, 'month').toDate();

    logger.info('Fetching transfers for %s..%s', fromDate, toDate);
    const feedNames = args.feeds ? args.feeds.split(',') : null;
    await puller.build(fromDate, toDate, feedNames, db);
    await db.doPostActions();

    logger.info('Notifying subscribers');
    await Pubsub.publish('transfers_list', 'updated');
    await Pubsub.quit();
    Sentry.captureEvent({
      message: "finish"
    });
  } catch (e) {
    Sentry.captureException(e);
  } finally {
    logger.info("Flushing database");
    await db.close();
  }
}



main().then(() => process.exit(0)).catch((err) => {
  console.error(err);
  process.exit(-1);
});
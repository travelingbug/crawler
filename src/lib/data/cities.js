const Airports = require('./airports');
const logger = require('../../logger')('Cities');
const geolib = require('geolib');

const _cities = [];
let _topCities = [];

function _fillUpAllCities() {
    for (const [, country] of Object.entries(Airports.getCountries())) {
        for (const [, city] of Object.entries(country)) {
            _cities.push(city);
        }
    }
    _cities.sort((a, b) => (a.name.localeCompare(b.name)));


}

function _fillUpTopCities() {
    const topCities = require('./top_cities.json');
    _topCities = topCities.map(i => (Airports.getCountries()[i.country][i.name]));
}

_fillUpAllCities();
_fillUpTopCities();



function getPopularity(city) {
    let i = 0;
    for (; i < _topCities.length; i++) {
        const t = _topCities[i];
        if (t.name === city.name && t.country == city.country) {
            return i;
        }
    }
    return i;
}

function filter(needle, lat, lon, maxDistance = null /* m */, limit = Number.MAX_SAFE_INTEGER, sort = null, cities = null) {
    const results = [];
    if (!cities) {
        cities = _cities;
    }

    const query = needle && needle.toLowerCase();
    const distanceRequired = (sort && sort.indexOf("distance") !== -1) || maxDistance;

    for (const city of cities) {
        if (query && (
            !city.name.toLowerCase().startsWith(query) &&
            !city.country.toLowerCase().startsWith(query) &&
            (city.name + '/' + city.country).toLowerCase() !== query)) {
            continue;
        }

        const o = {...city};

        if (distanceRequired) {
            const distance = geolib.getDistance(
                { latitude: lat, longitude: lon },
                { latitude: city.lat, longitude: city.lon }
            );

            if (maxDistance && distance > maxDistance) {
                continue;
            }

            o.distance = distance;
        }

        results.push(o);
    }

    if (typeof sort === 'string') {
        for (const i of sort.split(',')) {
            switch (i) {
                case "distance":
                    results.sort((a, b) => (a.distance - b.distance));
                break;

                case "popularity":
                    results.sort((a, b) => (getPopularity(a) - getPopularity(b)));
                break;

                default:
                    console.error("Invalid order name", i);
                break;
            }
        }
    }
    return results.slice(0, Math.min(limit, results.length));
}

module.exports = {
    filterTransfer(transfers) {
        const activeAirports = Airports.filter(transfers);
        const activeCities = [];
        for (let airportCode in activeAirports) {
            const airportInfo = activeAirports[airportCode];
            activeCities.push({
                name: airportInfo.city,
                country: airportInfo.country,
                lat: airportInfo.lat,
                lon: airportInfo.lon
            });
        }
        return activeCities;
    },

    getCity(country, name) {
        const city = Airports.getCountries()[country][name];
        if (!city) {
            logger.warn("getCity() city %s/%s not found", name, country);
        }
        return city;
    },
    getAllCities: () => _cities,
    getTopCities: () => _topCities,
    getCountries: () => Airports.getCountries(),
    filter
};

const csvSync = require('./csv');
const path = require('path');
const logger = require('../../logger')('airports');
/*
0 Airport ID	Unique OpenFlights identifier for this airport.
1 Name	Name of airport. May or may not contain the City name.
2 City	Main city served by airport. May be spelled differently from Name.
3 Country	Country or territory where airport is located. See countries.dat to cross-reference to ISO 3166-1 codes.
4 IATA	3-letter IATA code. Null if not assigned/unknown.
5 ICAO	4-letter ICAO code.
6 Latitude	Decimal degrees, usually to six significant digits. Negative is South, positive is North.
7 Longitude	Decimal degrees, usually to six significant digits. Negative is West, positive is East.
8 Altitude	In feet.
9 Timezone	Hours offset from UTC. Fractional hours are expressed as decimals, eg. India is 5.5.
10 DST	Daylight savings time. One of E (Europe), A (US/Canada), S (South America), O (Australia), Z (New Zealand), N (None) or U (Unknown). See also: Help: Time
11 Tz database time zone	Timezone in 'tz' (Olson) format, eg. 'America/Los_Angeles'.
12 Type	Type of the airport. Value 'airport' for air terminals, 'station' for train stations, 'port' for ferry terminals and 'unknown' if not known. In airports.csv, only type=airport is included.
13 Source	Source of this data. 'OurAirports' for data sourced from OurAirports, 'Legacy' for old data not matched to OurAirports (mostly DAFIF), 'User' for unverified user contributions. In airports.csv, only source=OurAirports is included.
*/

const _airports = {};
const countries = {};

function build(data) {
    for (let d of data) {
        if (d[4] === undefined || d[4] === '\\N' || (d[12] !== 'airport' && d[12] !== 'unknown') || d[2].length === 0 /* city is not defined */ || d[3].length == 0 /* country is not defined */ ) {
            continue;
        }

        // build countries index
        const city = d[2];
        const country = d[3];
        const latitude = d[6];
        const longitude = d[7];
        const type = d[12];

        // build airports index
        _airports[d[4]] = {
            name: d[1],
            city: d[2],
            country: d[3],
            lat: latitude,
            lon: longitude
        };

        if (!(country in countries)) {
            countries[country] = {};
        }

        const data = city in countries[country] ? countries[country][city] : {
            name: city,
            country: country,
            airports: [],
            lat: latitude,
            lon: longitude
        };

        if (type === 'airport') {
            data.airports.push(d[4]);
        } else if (type === 'station') {
            data.lat = latitude;
            data.lon = longitude;
        }

        countries[country][city] = data;
    }
}

build(csvSync(path.join(__dirname, '..', '..', '..', 'res', 'airports.dat')));

module.exports = {
    getAirports: () => _airports,
    getCountries: () => countries,
    getCityByAirportIATA(airportIATA) {
        const airport = _airports[airportIATA];
        if (!airport) {
            logger.warn('airport %s has not been found in openflights database', airportIATA);
            return null;
        }
        return countries[airport.country][airport.city];
    },
    /* Filter the airports dict according to transfers data.
       For example, _airports contains Minsk airport, but the transfers data doesn't contain any transfers from or to Minsk
    */
    filter: (transfers) => {
        const airports = {};
        for (const transfer of transfers) {
            for (const airportCode of [transfer.origin, transfer.destination]) {
                if (airportCode && !airports[airportCode])
                    if (_airports[airportCode]) {
                        airports[airportCode] = _airports[airportCode];
                    } else {
                        logger.warn('there are no information about %s', airportCode);
                    }
            }
        }

        return airports;
    }
};

const csvSync = require('./csv');
const path = require('path');

/*
0 Airline	2-letter (IATA) or 3-letter (ICAO) code of the airline.
1 Airline ID	Unique OpenFlights identifier for airline (see Airline).
2 Source airport	3-letter (IATA) or 4-letter (ICAO) code of the source airport.
3 Source airport ID	Unique OpenFlights identifier for source airport (see Airport)
4 Destination airport	3-letter (IATA) or 4-letter (ICAO) code of the destination airport.
5 Destination airport ID	Unique OpenFlights identifier for destination airport (see Airport)
6 Codeshare	"Y" if this flight is a codeshare (that is, not operated by Airline, but another carrier), empty otherwise.
7 Stops	Number of stops on this flight ("0" for direct)
8 Equipment	3-letter codes for plane type(s) generally used on this flight, separated by spaces
*/
const data = csvSync(path.join(__dirname, '..', '..', '..', 'res', 'routes.dat'), '\r\n');


module.exports = {
    getAirlineDirections: (airlineIATA) => {
        const directions = {};
        for (let d of data) {
            const airline = d[0];
            const origin = d[2];
            const destination = d[4];

            if (airline === airlineIATA && d[7] === 0) {
                if (!directions[origin])
                    directions[origin] = [];
                directions[origin].push(destination);
            }
        }
        return directions;
    },
    getAirlinesInfo: function() {
        return data;
    }
};

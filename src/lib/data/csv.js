const fs = require('fs');
const parse = require('csv-parse/lib/sync');

function load(name, new_line) {
    const data = fs.readFileSync(name);
    return parse(data, {skip_empty_lines: true, auto_parse: true, escape: '\\', quote: '"', rowDelimiter: new_line || '\n'});
}

module.exports = load;
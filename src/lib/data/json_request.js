const request = require('request');


module.exports = {
    get: (options) => {
        return new Promise((resolve, reject) => {
            request.get(options, (err, res, body) => {
                if (err || res.statusCode !== 200) {
                    return reject(err || res);
                } else {
                    return resolve(JSON.parse(body));
                }
            });
        });
    },
    post: async (url, postData) => {
        return new Promise((resolve, reject) => {
            request.post(url, {json: postData}, (err, res, body) => {
                console.log(url, postData, typeof(body), body, res.statusCode);
                if (err || res.statusCode !== 200) {
                    return reject(err || res);
                } else {
                    return resolve(body);
                }
            });
        });
    }
};
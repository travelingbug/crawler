const logger = require('../logger')();
const cliProgress = require('cli-progress');
const Sentry = require('@sentry/node');

module.exports = {
    build: async (fromDate, toDate, feedNames=null, db) => {
        const startTime = Date.now();
        if (!feedNames) {
            feedNames = ['ryanair', 'wizzair' /*, 'norwegian' */];
        }

        const feeds = feedNames.map(feedName => require(`./feeds/${feedName}`));

            // create new container
        const multibar = new cliProgress.MultiBar({
            clearOnComplete: false,
            hideCursor: true
        }, cliProgress.Presets.shades_grey);

        const promises = feeds.map(async (Feed) => {
            const feed = new Feed(db);
            const progressBar = process.env.PROGRESS ? multibar.create(100, 0, feed.name) : null;

            try {
                await feed.getTransfers(fromDate, toDate, (value, total) => {
                    if (progressBar) {
                        progressBar.setTotal(total);
                        progressBar.update(value);
                    }
                });
            } catch (e) {
                logger.error('%s: %s\n%s', feed.name, e, e.stack);
                Sentry.captureException(e);
                feed.abort();
            } finally {
                logger.info("%s crawler finished it's work, time: %ds", feed.name, (Date.now() - startTime) / 1000);
            }
        });

        return Promise.all(promises).then(results => {
            return results.reduce((a,b) => a.concat(b), []);
        });
    }
};
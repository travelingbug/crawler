const MongoClient = require("mongodb").MongoClient;
const logger = require("../logger")("mongodb");

const MAX_ITEMS = 128;
const TMP_COLLECTION = "transfers_";
const COLLECTION = "transfers";

class DBInterface {
  constructor() {
    this.transfers = [];
  }

  async init(uri) {
    this.db = await MongoClient.connect(uri);

    try {
      await this.db.collection(TMP_COLLECTION).drop();
    } catch (e) {
      logger.debug("%s doesn't exist", TMP_COLLECTION);
    }

    await this.db.collection(TMP_COLLECTION).createIndex({
      origin: "text"
    });
    await this.db.collection(TMP_COLLECTION).createIndex({
      departureTime: 1
    });
    await this.db.collection(TMP_COLLECTION).createIndex({
      arrivalTime: 1
    });
    logger.info("db initiated");
  }

  async insert(...objects) {
    this.transfers = this.transfers.concat(objects);
    if (this.transfers.length > MAX_ITEMS) {
      await this.flush();
    }
  }

  async flush() {
    if (this.transfers.length > 0) {
      const transfers = this.transfers;
      this.transfers = [];
      await this.db.collection(TMP_COLLECTION).insertMany(transfers);
    }
  }

  async doPostActions() {
    logger.debug("doPostActions() flushing");
    await this.flush();
    try {
      await this.db.collection(COLLECTION).drop();
    } catch (e) {
      console.debug("collection doesn't exist");
    }
    logger.debug("doPostActions() renaming collections");
    await this.db.collection(TMP_COLLECTION).rename(COLLECTION, true);
  }

  async get(filter_obj = {}) {
    return await this.db.collection(COLLECTION).find(filter_obj).toArray();
  }

  async close() {
    logger.debug("close()");

    if (this.db) {
      await this.flush();
      await this.db.close();
    }
  }
}

module.exports = DBInterface;


const moment = require("moment-timezone");
const Nightmare = require("nightmare");
require('nightmare-webrequest-addon');

const AirlinesFeed = require("../airlines_feed.js");

process.env["QT_QPA_PLATFORM"] = "offscreen";

class Feed extends AirlinesFeed {
  constructor(db) {
    super(db, 20);

    this.apiEndpoint = null;
    this.apiBase = null;
    this.total = 0;
  }

  get name() {
    return "Wizzair";
  }

  get nameIATA() {
    return "W6";
  }

  async getTransfersMap() {
    const mapApiUrl = `${
      this.apiBase
    }/asset/map?languageCode=en-gb&forceJavascriptOutput=true`;
    const payload = await this.GET(mapApiUrl);
    this.logger.debug("transfersMap: %s", JSON.stringify(payload));
    return payload.cities;
  }

  async updateAPIEndpoint() {
    const body = await this.GET("https://wizzair.com/static_fe/metadata.json");
    this.logger.info("endpoint %s %s", body, body.apiUrl);
    this.apiEndpoint = `${body.apiUrl}/search/timetable`;
    this.apiBase = body.apiUrl;
    return this.apiEndpoint;
  }

  async updateCookies() {
    this.logger.debug("updateCookies() opening web session");
    const nightmare = new Nightmare({show: false});
    const cookies = await nightmare
      .on('console', (log, msg) => {
        this.logger.debug("nightmare: %s", JSON.stringify(msg));
      })
      .onBeforeRequest((details, cb) => {
          return cb({cancel: details.url.match(/.*\.hotjar.com.*/)});
        }
      )
      .goto("https://wizzair.com/en-gb/flights/map#/")
      .evaluate(() => {
        console.log("evaluated");
      })
      .cookies.get();

    this.cookies = cookies
      .map(c => {
        return `${c["name"]}=${c["value"]}`;
      })
      .join("&");
    this.logger.debug("received cookies: %s", this.cookies);
    return nightmare.end();
  }

  async getTransfers(fromDate, toDate, progressCallback) {
    await this.updateAPIEndpoint();
    await this.updateCookies();

    const transfersMap = await this.getTransfersMap();
    this.logger.info("got a transfers map");

    let doneCount = 0;
    const tasks = [];
    for (let srcAirport of transfersMap) {
      for (let dstAirport of srcAirport.connections) {
        if (!this.isRequested(srcAirport.iata, dstAirport.iata)) {
          const task = async () => {
            let r = await this.getTransfersForDirection(
              srcAirport.iata,
              dstAirport.iata,
              fromDate,
              toDate
            );
            progressCallback && progressCallback(++doneCount, tasks.length);
            return r;
          };
          tasks.push(task());
        } else {
          this.logger.warn("%s->%s %s-%s was requested\n", srcAirport.iata, dstAirport.iata, fromDate, toDate);
        }
      }
    }

    return Promise.all(tasks);
  }

  async getTransfersForDirection(origin, destination, fromDate, toDate) {
    this.logger.debug("getTransfersForDirection %s->%s %s...%s", origin, destination, fromDate, toDate);
    const months = this.getMonthsList(fromDate, toDate);
    return Promise.all(
      months.map(async date => {
        const from = moment(date).tz("Europe/Budapest").format("YYYY-MM-DD");
        const to = moment(date)
          .add(1, "month")
          .format("YYYY-MM-DD");

        const request_data = {
          uri: this.apiEndpoint,
          body: {
            flightList: [
              {
                departureStation: origin,
                arrivalStation: destination,
                from: from,
                to: to
              },
              {
                departureStation: destination,
                arrivalStation: origin,
                from: from,
                to: to
              }
            ],
            infrantCount: 0,
            adultCount: 1,
            childCount: 0,
            priceType: "regular"
          },
          headers: {
            cookie: this.cookies,
            origin: "https://wizzair.com",
            referer: "https://wizzair.com/",
            "user-agent":
              "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36"
          }
        };

        var body = await this.POST(request_data);

        if (!body.outboundFlights || !body.returnFlights) {
          return [];
        }

        for (let f of body.outboundFlights.concat(body.returnFlights)) {
          if (!f.price ||  !f.price.amount) {
            continue;
          }

          if (!await this.pushTransfers({
              origin: f.departureStation,
              destination: f.arrivalStation,
              departureTime: new Date(f.departureDates[0]),
              arrivalTime: new Date(f.departureDates[0]),
              price: (f.price && f.price.amount) || null,
              currency: (f.price && f.price.currencyCode) || null,
              airline: this.nameIATA,
              type: "flight"
            })) {
              this.logger.warn("duplicate transfer for search request %s->%s %s-%s", origin, destination, fromDate, toDate);
            } else {
              this.logger.debug("add transfer for search request %s->%s %s-%s", origin, destination, fromDate, toDate);
            }
        }
      })
    );
  }
}

module.exports = Feed;

const moment = require('moment');
const AirlinesFeed = require('../airlines_feed.js');

class Feed extends AirlinesFeed {
    get name() {
        return 'Ryanair';
    }

    get nameIATA() {
        return 'FR';
    }

    constructor(db) {
        super(db, 30);
        this.total = 0;
        this.apiEndpoint = "https://services-api.ryanair.com";
    }

    async getTransfers(fromDate, toDate, progressCallback) {
        let stat = { done: 0, total: 0 };

        return this.GET('https://www.ryanair.com/api/booking/v4/en-us/res/stations')
            .then(originAirports => {
                return Promise.all(Object.keys(originAirports).map((originAirport) => {
                    return this.GET(`${this.apiEndpoint}/timtbl/3/schedules/${originAirport}/periods`)
                        .then(destinationAirports => {
                            stat.total += Object.values(destinationAirports).length;

                            return Promise.all(Object.keys(destinationAirports).map(async (destinationAirport) => {
                                if (!this.isRequested(originAirport, destinationAirport)) {
                                    return await this.getTransfersForDirection(originAirport, destinationAirport, fromDate, toDate);
                                }
                                progressCallback && progressCallback(++stat.done, stat.total);
                            }));
                        });
                }));
        });
    }

    async getTransfersForDirection(origin, destination, fromDate, toDate) {
        const months = this.getMonthsList(fromDate, toDate);
        for (let date of months) {
            // link are reverse-engineered from https://www.ryanair.com/gb/en/cheap-flights/dublin-to-london-luton?out-from-date=2019-02-24&out-to-date=2020-02-24&budget=150
            const url = `${this.apiEndpoint}/farfnd/3/roundTripFares/${origin}/${destination}/cheapestPerDay?`
                + `inboundMonthOfDate=${moment(date).format('YYYY-MM-DD')}&`
                + `market=en-gb&`
                + `outboundMonthOfDate=${moment(date).format('YYYY-MM-DD')}`;
            const body = await this.GET(url);

            for (let f of body.outbound.fares || []) {
                if (f.price) {
                    await this.pushTransfers({
                        origin: origin,
                        destination: destination,
                        price: f.price.value,
                        currency: f.price.currencyCode,
                        departureTime: new Date(f.day),
                        arrivalTime: new Date(f.day),
                        airline: this.nameIATA,
                        type: 'flight'
                    });
                }
            }

            for (let f of body.inbound.fare || []) {
                if (f.price) {
                    await this.pushTransfers({
                        origin: destination,
                        destination: origin,
                        price: f.price.value,
                        currency: f.price.currencyCode,
                        departureTime: new Date(f.day),
                        arrivalTime: new Date(f.day),
                        airline: this.nameIATA,
                        type: 'flight'
                    });
                }
            }
        }
    }
}

module.exports = Feed;
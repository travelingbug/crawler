const AirlinesFeed = require('../airlines_feed.js');

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(() =>  {
            resolve();
        }, ms);
    });
}

function generateTransfer() {
    const departureTime = Date.now() + Math.random() * 24 * 60 *1000;
    return {
        "origin" : "ACE",
        "destination" : "SCQ",
        "price" : Math.random() * 100,
        "currency" : "EUR",
        "departureTime" : departureTime,
        "arrivalTime" : departureTime + Math.random() * 60000,
        "airline" : "FR",
        "type" : "flight"
    };
}


class Feed extends AirlinesFeed {
    get name() {
        return 'Ryanair';
    }

    get nameIATA() {
        return 'FR';
    }

    async getTransfers(fromDate, toDate, progressCallback) {
        const total = 100;

        for (let i = 0; i < total; i++) {
            await sleep(Math.random() * 1000);
            progressCallback && progressCallback(i+1, total);
            this.pushTransfers(generateTransfer());
        }

    }
}

module.exports = Feed;


const routes = require('../data/routes');
const airports = require('../data/airports');
const moment = require('moment');
const request = require('request-promise-native');
const crypto = require('crypto');

var RateLimiter = require('request-rate-limiter');

function strip(content) {
    content = content.toString();
    if (content.charCodeAt(0) === 0xFEFF) {
        content = content.slice(1);
    }
    return content;
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(() =>  {
            resolve();
        }, ms);
    });
}
class AirlinesFeed {
    constructor(db, rate = 50) {
        this.db = db;
        this.transfersSet = new Set();
        this.requested = new Object();
        this.aborted = false;
        this.limiter = new RateLimiter({
            rate: rate,
            interval: 10,
            backoffTime: 1,
            maxWaitingTime: 3600
        });
        this.logger = require('../../logger')(this.name);
    }

    /*  This function returns true if the request for origin-destination or destination-origin is not yet made
        This check allow avoid double requesting/fetching the same flights or transtitions, because the request
        'origin-destination' returns also 'destination-origin' flights. For example, if we request VNO-OSL flights
        we will receive also OSL-VNO flights (return flights).
     */
    isRequested(origin, destination) {
        let a, b;
        if (origin < destination) {
            a = origin;
            b = destination;
        } else {
            a = destination;
            b = origin;
        }

        if (!(a in this.requested)) {
            this.requested[a] = [];
        }

        if (this.requested[a].includes(b)) {
            return true;
        }

        this.requested[a].push(b);
        return false;
    }

    getMonthsList(fromDate, toDate) {
        const months = [fromDate];
        for (let i = 1; moment(fromDate).add(i, 'month') < moment(toDate); i++) {
            months.push(moment(fromDate).add(i, 'month').startOf('month').toDate());
        }

        return months;
    }
    getTransfers(fromDate, toDate, progressCallback) {
        const promises = [];

        this.logger.debug('Fetching %s on %s-%s', this.name, fromDate, toDate);
        const directions = routes.getAirlineDirections(this.nameIATA);
        this.logger.debug(`${this.name} has ${Object.keys(directions).length} routes`);

        let doneCount = 0;

        for (let origin in directions) {

            for (let dst of directions[origin]) {
                if (!this.isRequested(origin, dst))
                    promises.push(async () => {
                        let r = await this.getTransfersForDirection(origin, dst, fromDate, toDate);
                        progressCallback && progressCallback(++doneCount, promises.length);
                        return r;
                    });
            }
        }

        return Promise.all(promises).then(results => results.reduce((a, b) => a.concat(b), []));
    }

    getTransfersForDirection() {
        throw new Error('not implemented');
    }

    async GET(url) {
        if (this.aborted) {
            throw new Error("HTTP GET " + url + " has been aborted");
        }

        const opt = {
            uri: url,
            json: true
        };
        const backoff = await this.limiter.request();
        if (this.aborted) {
            backoff();
            throw new Error("HTTP GET " + url + " has been aborted");
        }

        let res = null;
        let tries = 0;

        while (res === null && !this.aborted) {
            try {
                res = await request.get(opt);
                if (typeof (res) === 'string') {
                    res = JSON.parse(strip(res));
                }
            } catch (e) {
                tries += 1;
                if (tries == 3) {
                    throw new Error("HTTP GET " + url + " " + e.message + ":\n" + e.stack);
                }
            } finally {
                backoff();
            }
        }

        this.logger.debug('GET %s: %s', url, JSON.stringify(res));
        return res;
    }

    async POST(request_data) {

        const opt = {
            ...request_data,
            json: true
        };

        if (this.aborted) {
            backoff();
            throw new Error(`HTTP POST "${request_data.uri}" ${JSON.stringify(opt.body)} has been aborted`);
        }

        let res = null;

        const backoff = await this.limiter.request();
        if (this.aborted) {
            backoff();
            throw new Error(`HTTP POST "${request_data.uri}" ${JSON.stringify(opt.body)} has been aborted`);
        }

        let err = null;

        for (let i = 0; i < 3; i++) {
            try {
                res = await request.post(opt);
                err = null;
                break;
            } catch (e) {
                this.logger.warn("POST request error. Sleeping...");
                await sleep(Math.floor(Math.random()*10000));
                err = e;
            }
        }

        backoff();

        if (err) {
            throw new Error(`HTTP POST "${request_data.uri}" ${JSON.stringify(opt.body)}: ${err.message}\n${err.stack}`);
        }

        this.logger.debug('POST %s: %s', opt.uri, JSON.stringify(opt.body), JSON.stringify(res));
        return res;
    }

    async pushTransfers(transfer) {
        if (!airports.getCityByAirportIATA(transfer.origin)) {
            this.logger.warn("invalid departure airport %s",  JSON.stringify(transfer));
            return;
        }
        if (!airports.getCityByAirportIATA(transfer.destination)) {
            this.logger.warn("invalid arival airport %s", JSON.stringify(transfer));
            return;
        }

        const hash = crypto.createHash('md5');
        const serializedTransfer = JSON.stringify(transfer);
        const id = hash.update(serializedTransfer).digest().toString('hex');

        if (!this.transfersSet.has(id)) {
            this.transfersSet.add(id);
            this.logger.debug("added a transfer %s: %s", id, serializedTransfer);
            await this.db.insert(transfer);
            return true;
        } else {
            this.logger.warn("drop a duplicate transfer %s: %s", id, serializedTransfer);
            return false;
        }
    }

    abort() {
        this.aborted = true;
    }
}

module.exports = AirlinesFeed;
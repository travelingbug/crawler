const jsonRequest = require('../../data/json_request');
const storage = require('../../../storage');
const logger = require('../../../logger')();
const moment = require('moment');
const API_URL_BASE = 'http://api.ecolines.net/v1';

class Feed {
    get name() {
        return 'Ecolines';
    }

    getCity(struct) {
        //title  could be with description like: Minks (Centralny)
        return struct.title.split('(')[0];
    }

    getMonthMoments(fromDate, toDate) {
        const moments = [];
        for (let i = 0; moment(fromDate).add(i, 'month') < moment(toDate); i++) {
            moments.push(moment(fromDate).add(i, 'month'));
        }
        return moments;
    }

    getTransfers(fromDate, toDate) {
        const monthMoments = this.getMonthMoments(fromDate, toDate);
        const directionsStorageName = `${this.name}.directions`;

        return storage.get(directionsStorageName)
            .then(directions => {
                if (directions) {
                    return directions;
                } else {
                    return jsonRequest.get(`${API_URL_BASE}/directions.json`)
                        .then(directions => {
                            logger.debug('received directions');
                            return storage.set(directionsStorageName, directions).then(() => directions);
                        });
                }
            }).then(directions => {
                return Promise.all(directions.map(d => {
                    // https://gist.github.com/derhuerst/c76db8e9216b686b0262857cc9abd16e
                    return jsonRequest.get(`${API_URL_BASE}/timetable.json?origin=${d.origin.id}&destination=${d.destination.id}`) //request timetable
                        // received timetable
                        .then(timetable => {
                            logger.debug('received timetable for %s-%s', d.origin.title, d.destination.title);

                            return Promise.all(monthMoments.map(currentMoment => {
                                return jsonRequest.get(`https://booking.ecolines.net/ajax/dates?origin=${d.origin.id}&destination=${d.destination.id}&year=${currentMoment.year()}&month=${currentMoment.month()}`); //request dates for each month
                                // received dates -> reduce dates for few months
                            })).then(datesArray => {
                                logger.debug('received dates for %s-%s', d.origin.title, d.destination.title);
                                return datesArray.reduce((a, b) => a.concat(b), []);
                            }).then(dates => {
                                return { dates: dates, timetable: timetable };
                            });
                        }).then(args => {
                            args.timetable.movements.map(movement => {
                                return args.dates.map(date => {
                                    return {
                                        type: 'bus',
                                        carrier: this.name,
                                        departureTime: moment(date).add(movement.departure).toDate(),
                                        arrivalTime: moment(date).add(movement.arival).toDate()
                                    };
                                });
                            }).reduce((a, b) => a.concat(b), []);
                        });
                })).then(result => result.reduce((a, b) => a.concat(b), []));
            });
    }
}

module.exports = Feed;
const redis = require('redis');
const logger = require('./logger')('redis-pubsub');
const config = require('./config');

const client = redis.createClient({host: config.REDIS_URL.split(':')[0], port: config.REDIS_URL.split(':')[1], return_buffers: true});

client.on('error', function (err) {
    logger.error('%s', err.message);
});

if (config.REDIS_PASSWORD) {
    client.auth(config.REDIS_PASSWORD, function(err) {
        if (err)
            logger.error(err.message);
    });
}

module.exports = {
  subscribe: function(channel, cb) {
    logger.info('subscribed to %s', channel);
    client.subscribe(channel);
    return client.on('message', (ch, msg) => {
      if (ch.toString() === channel) {
        logger.debug(ch.toString(), msg.toString());
        cb(ch.toString(), msg.toString());
      }
    });
  },

  publish: function(channel, message) {
    client.publish(channel, message);
  },

  quit: function() {
    client.quit();
  }
};

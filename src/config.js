const fs = require('fs');
const path = require('path');

let jsonConfig;
try {
    jsonConfig = JSON.parse(fs.readFileSync(path.join(path.dirname(__filename), 'config.json')));
} catch (e) {
    jsonConfig = {};
}

const params = {
    REDIS_URL: process.env.OPENSHIFT_REDIS_HOST || process.env.REDIS_URL || '127.0.0.1:6379',
    MONGODB_URL: process.env.MONGODB_URL || 'mongodb://localhost:27017/travelingbug',
    SENTRY_DSN: process.env.SENTRY_DSN,
};

Object.assign(params, jsonConfig);

module.exports = params;

const { createLogger, format, transports } = require('winston');

const myFormat = format.printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});
module.exports = function (name) {
  return new createLogger({
    level: process.env.LOG_LEVEL || 'info',
    format: format.combine(
      format.colorize(),
      format.label({label: name}),
      format.colorize(),
      format.timestamp(),
      format.splat(),
      format.simple(),
      myFormat
    ),
    transports: [
      new transports.Console()
    ]
  });
};
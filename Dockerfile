FROM node:14.2
MAINTAINER Dennis Vashchuk <dennis.vashchuk@gmail.com>

RUN apt update -yq && apt install -yq xvfb libxcomposite1 libxcursor1 libxi6 libxtst6 libnss3 libxss1 libasound2 libasound2 libgtk-3-0 libgconf-2-4
WORKDIR /app
ENV DEBUG nightmare:*,electron:*
COPY src src
COPY res res
COPY package.json package.json

RUN yarn install
RUN useradd -m service
USER service:service
CMD bash -c 'while :; do xvfb-run yarn start && sleep 4h; done;'
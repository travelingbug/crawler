# Intro
This microservice pulls data from airline companies:
- Ryanair
- Wizzair
- Norwegian (does't work after latest changes)

It is not good way to do things, but airlines still don't provide a public API to access this data.

Software Developers, Engineers and DevOps, let's stop this closed-API mess, let's do open API and make life better for everyone! It will open a lot of opportunities for everyone!

# Development
## Dependencies
```sh
yarn install --dev
yarn start
```

## Docker Compose
```sh
yarn run compose
```
